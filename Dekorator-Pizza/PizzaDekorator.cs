﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator_Pizza
{
    abstract class PizzaDekorator : Pizza
    {
        public Pizza tempPizza { get; set; }

        public PizzaDekorator(Pizza tempPizza)
        {
            this.tempPizza = tempPizza;
        }
        public virtual double getCost()
        {
            return tempPizza.getCost();
        }

        public virtual string getDescription()
        {
            return tempPizza.getDescription();
        }
    }
}
