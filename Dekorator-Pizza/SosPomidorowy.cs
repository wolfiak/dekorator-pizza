﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator_Pizza
{
    class SosPomidorowy : PizzaDekorator
    {
        public SosPomidorowy(Pizza tempPizza) : base(tempPizza)
        {
            Console.WriteLine("Niech ja dodam troche sosou");
        }

        public override string getDescription()
        {
            return tempPizza.getDescription() + " Sos pomidorowy";
        }
        public override double getCost()
        {
            return tempPizza.getCost()+0.5;
        }
    }
}
