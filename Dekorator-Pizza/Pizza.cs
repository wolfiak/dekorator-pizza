﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator_Pizza
{
    interface Pizza
    {
        string getDescription();
        double getCost();
    }
}
