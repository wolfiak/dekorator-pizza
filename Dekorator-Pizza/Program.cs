﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator_Pizza
{
    class Program
    {
        static void Main(string[] args)
        {
            Pizza pizza = new SosPomidorowy(new Mozzarella(new PlainPizza()));
            Console.WriteLine("Dodatki: " + pizza.getDescription());
            Console.WriteLine("TOTAL: " + pizza.getCost());
            Console.WriteLine("KOniec");
        }
    }
}
