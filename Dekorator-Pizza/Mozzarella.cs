﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dekorator_Pizza
{
    class Mozzarella : PizzaDekorator
    {
        public Mozzarella(Pizza tempPizza) : base(tempPizza)
        {
            Console.WriteLine("Dodawanie mozzarelli");
        }
       public override string getDescription()
        {
            return tempPizza.getDescription() + "Mozzarella";
        }
        public override double getCost()
        {
            return tempPizza.getCost() + 0.5;
        }
    }
}
